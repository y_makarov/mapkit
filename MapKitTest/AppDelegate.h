//
//  AppDelegate.h
//  MapKitTest
//
//  Created by Yevhen Makarov on 05.01.18.
//  Copyright © 2018 Yevhen Makarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

