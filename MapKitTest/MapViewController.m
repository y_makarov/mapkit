//
//  ViewController.m
//  MapKitTest
//
//  Created by Yevhen Makarov on 05.01.18.
//  Copyright © 2018 Yevhen Makarov. All rights reserved.
//

#import "MapViewController.h"
#import "MapMarker.h"

@interface MapViewController () <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, readonly) MapMarker *homeMarket;
@property (nonatomic, readonly) CLLocationManager *locationManager;

@end

@implementation MapViewController
@synthesize homeMarket = _homeMarket;
@synthesize locationManager = _locationManager;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.showsScale = YES;
    self.mapView.delegate = self;
    [self.mapView addAnnotation:self.homeMarket];
    self.mapView.showsUserLocation = YES;
    [self.locationManager requestWhenInUseAuthorization];
}
- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [CLLocationManager new];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.distanceFilter = 1;
    }
    return _locationManager;
}

- (MapMarker *)homeMarket {
    if (!_homeMarket) 
        _homeMarket = [MapMarker homeMarket];
        return _homeMarket;
    }

- (void)zoomByFactor:(CGFloat)factor {
    MKCoordinateRegion region = self.mapView.region;
    MKCoordinateSpan span = region.span;
    span.latitudeDelta = factor;
    span.longitudeDelta = factor;
    region.span = span;
    [self.mapView setRegion:region animated:YES];
}
#pragma mark - Map View Delegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if (annotation == self.homeMarket) {
        static NSString *markerID = @"MarkerId";
        MKPinAnnotationView *pin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:markerID] ?: [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:markerID];
        pin.canShowCallout = YES;
        pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        return pin;
    }
    return nil;
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    if (view.annotation == self.homeMarket) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:view.annotation.title message:view.annotation.subtitle preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Navigate" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            MKPlacemark *place = [[MKPlacemark alloc] initWithCoordinate:view.annotation.coordinate addressDictionary:nil];
            MKMapItem *mapItem = [[MKMapItem alloc]initWithPlacemark:place];
            [mapItem openInMapsWithLaunchOptions:@{MKLaunchOptionsDirectionsModeKey :MKLaunchOptionsDirectionsModeDriving} ];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


- (IBAction)zoomInDidClick:(id)sender {
    [self zoomByFactor:0.9];
}
- (IBAction)zoomOutDidClick:(id)sender {
    [self zoomByFactor:5];
}

- (IBAction)whereImDidClick:(id)sender {
    if (self.mapView.userLocation) {
        [self.mapView showAnnotations:@[self.mapView.userLocation] animated:YES];
    }
}


@end
