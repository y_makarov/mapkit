//
//  MapMarker.m
//  MapKitTest
//
//  Created by Yevhen Makarov on 05.01.18.
//  Copyright © 2018 Yevhen Makarov. All rights reserved.
//

#import "MapMarker.h"



@implementation MapMarker

+ (instancetype)marketWithTitle:(NSString *)title subtitle:(NSString *)subtitle andCooardinate:(CLLocationCoordinate2D)cooardinate {
    MapMarker *market = [[self alloc]init];
    market.title = title;
    market.subtitle = subtitle;
    market.coordinate = cooardinate;
    return market;
     //48.520171 32.254777
}

+ (instancetype)homeMarket {
    return [MapMarker marketWithTitle:@"Alexandra" subtitle:@"Kalvaria utca 16" andCooardinate:CLLocationCoordinate2DMake(47.486594, 19.086458)];
}

@end
