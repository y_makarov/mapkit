//
//  MapMarker.h
//  MapKitTest
//
//  Created by Yevhen Makarov on 05.01.18.
//  Copyright © 2018 Yevhen Makarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapMarker : NSObject <MKAnnotation>
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic) CLLocationCoordinate2D coordinate;

+ (instancetype)marketWithTitle:(NSString *)title subtitle:(NSString *)subtitle andCooardinate:(CLLocationCoordinate2D)cooardinate;
+ (instancetype)homeMarket;


@end
