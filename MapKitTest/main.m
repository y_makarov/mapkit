//
//  main.m
//  MapKitTest
//
//  Created by Yevhen Makarov on 05.01.18.
//  Copyright © 2018 Yevhen Makarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
